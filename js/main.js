const obj = {
    a: 3
    , b: 1
    , e: 4
    , c: 0
    , d: 9
    , f: 11
    , h: 2
    , z: 6
}

console.log(obj);

var listNew = [];
var sortedListKey = [];
var sortedListValue = [];

for (var i in obj) {
    listNew.push({
        id: i
        , name: obj[i]
    });
}

function compareValues(a, b) {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
};

sortedListKey = listNew.sort(function (a, b) {
    return a.id > b.id;
});

console.log('Отсортированные ключи: ');

for (var i = 0; i < sortedListKey.length; i++) {
    console.log(sortedListKey[i].id);
}

console.log('Отсортированные ключ + значения: ');
for (var i = 0; i < sortedListKey.length; i++) {
    console.log(sortedListKey[i].id + '-' + sortedListKey[i].name);
}

sortedListValue = listNew.sort(compareValues);

console.log('Отсортированные значения: ');
for (var i = 0; i < sortedListValue.length; i++) {
    console.log(sortedListValue[i].name);
}
